#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define GREEN_LED_DDR	DDRG
#define GREEN_LED_PORT	PORTG
#define GREEN_LED	(1<<3)

volatile uint8_t led_intensity;

ISR(TIMER0_COMP_vect)
{
	GREEN_LED_PORT &= ~GREEN_LED;
}

ISR(TIMER0_OVF_vect)
{
	OCR0 = led_intensity;
	if (OCR0)
		GREEN_LED_PORT |= GREEN_LED;
}

static void init()
{
	GREEN_LED_DDR |= GREEN_LED;
	UCSR0B |= (1<<RXEN) | (1<<TXEN);
#define BAUD 9600
#include <util/setbaud.h>
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
#if USE_2X
	UCSR0A |= (1 << U2X);
#else
	UCSR0A &= ~(1 << U2X);
#endif

	TCCR0 |= (1<<WGM00) | (1<<WGM01) | (1<<CS02) ; // fast PWM
	TIMSK |= (1<<OCIE0) | (1<<TOIE0); // enable the interrupts
	sei();
}

int main(void)
{
	init();
	uint8_t d=1;

	while (1) {
		if (d) {
			led_intensity++;
			if (led_intensity >= 0x80)
				led_intensity++;
			if (led_intensity==0xff)
				d=0;
		} else {
			led_intensity--;
			if (led_intensity > 0x80)
				led_intensity--;
			if (!led_intensity) {
				d=1;
				UDR0='y';
				_delay_ms(500);
			}
		}
		_delay_ms(8);
	}
	return 0;
}
