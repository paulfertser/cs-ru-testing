#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/mman.h>
#include <inttypes.h>
#include <math.h>

void parse_data(uint8_t *p, double p0)
{
	uint32_t tm;
	uint16_t temp, humid, press;
	double time, temperature, humidity, pressure, altitude, kelvin_temp;
	uint16_t lat, lat_m, lon, lon_m;
	int16_t height;
	uint16_t co;

	tm = p[0] | (p[1] << 8) | (p[2] << 16);
	temp = p[3] | ((p[4] & 0xf0) << 4);
	press = (p[4] & 0x0f) | ((p[5] & 0xfc) << 2);
	humid = (p[5] & 0x03) | (p[6] << 2);
	time = tm / 1000.;
	temperature = temp * 0.0625;
	humidity = ((humid / 1023.) - 0.1515) / 0.00636 / (1.0546 - 0.00216*temperature);
	pressure = ((press / 1023.) + 0.095) / 0.009 * 1000;

	lat = (p[7] << 8) | p[8];
	lat_m = (p[9] << 8) | p[10];
	lon = (p[11] << 8) | p[12];
	lon_m = (p[13] << 8) | p[14];
	height = (p[15] << 8) | p[16];
	co = (p[17] << 8) | p[18];

	kelvin_temp = temperature + 273.15;
	altitude = (kelvin_temp/pow(pressure/p0, (8.31432*(-0.0065)/(9.80665*0.0289644))) - kelvin_temp)/(-0.0065);

	/*printf("%d\n", press);*/
	printf("%f %f %f %f %f %d %02x %02x.%02x%02x %02x %02x.%02x%02x %d\n", time, temperature, pressure, humidity, altitude, co,
	       lat >> 8, lat & 0xff, lat_m >> 8, lat_m & 0xff, lon >> 8, lon & 0xff, lon_m >> 8, lon_m & 0xff,
	       height);
}

int main(int argc, char *argv[])
{
	uint8_t *indata;
	size_t index, i;
	int fd;
	struct stat sb;
	double p0;

	if (argc != 4) {
		printf("Usage:\n");
		printf("%s <file> <r|f> <pressure>\n", argv[0]);
		printf("where\t<file> is the path to the file to process\n");
		printf("\t<r|f> is \"r\" for radio data or \"f\" for flash\n");
		printf("\t<pressure> is the pressure at the reference altitude\n\n");
		return 2;
	}
                                                                                                                                                           
	fd = open(argv[1], O_RDONLY);                                                                                                     
        fstat(fd, &sb);                                                                                                                   

	indata = mmap(NULL, sb.st_size, PROT_READ, MAP_SHARED, fd, 0);
	if (indata == MAP_FAILED) {
		fprintf(stderr, "Can't mmap the file\n");
		return 1;
	}
	
	p0 = atof(argv[3]);

	if (!strcmp(argv[2], "r")) {
		for (index=0; index < sb.st_size; index++) {
			if (indata[index] != 'P' ||
			    indata[index+1] != 'M' ||
			    indata[index+30] != '!')
				continue;
			parse_data(&indata[index+2], p0);
			index+=30;
		}
	} else {
		for (index=0x1fc00-256; indata[index]!=0xff; index-=256) {
			for (i=0; i<13*19; i+=19)
				parse_data(&indata[index+i], p0);
		}
	}
	
	close(fd);
	return 0;
}

