#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/boot.h>
#include <avr/eeprom.h>
#include "onewire.h"
#include "ds18x20.h"

// use AVCC for the reference
#define AREF (1<<REFS0)

uint8_t EEMEM first_boot_e;
uint8_t first_boot;
uint8_t write_pgm_now;

uint8_t time_eeprom;

volatile uint32_t time_ms;
volatile uint16_t temperature;
volatile uint16_t pressure;
volatile uint16_t humidity;

volatile uint16_t lat, lat_m, lon, lon_m, height;
volatile uint16_t co;

uint8_t id[OW_ROMCODE_SIZE];

#define BUF_SIZE 31

uint8_t output_buf[BUF_SIZE] = { 'P', 'M', 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				 '!' };
uint8_t output_index;

uint8_t pgm_buf[SPM_PAGESIZE];
uint8_t pgm_index;
uint32_t page_addr;

uint8_t read_scratchpad(uint8_t id[], uint8_t sp[], uint8_t n);

static inline uint16_t read_adc(uint8_t channel)
{
	ADMUX = channel | AREF;
	ADCSRA |= (1<<ADSC);

	while (!(ADCSRA & (1<<ADIF)))
		;
	ADCSRA |= (1<<ADIF);
	return ADC;
}

ISR(TIMER1_COMPA_vect)
{
	time_ms++;
	time_eeprom++;
	uint16_t cur_ms = time_ms % 1000;
	if (cur_ms == 0) {
		PORTG |= (1<<PG4);
		PORTG &= ~(1<<PG3);
	} else if (cur_ms == 13)
		PORTG |= (1<<PG3);
	else if (cur_ms == 994)
		PORTG &= ~(1<<PG4);
	else if (cur_ms == 997)
		co = read_adc(2);
	else if (cur_ms % 2)
		pressure = read_adc(0);
	else
		humidity = read_adc(1);

	if (time_eeprom == 300) {
		uint32_t t = time_ms;
		uint16_t tm = temperature, p = pressure, h = humidity;
		pgm_buf[pgm_index++] = t & 0xff;
		pgm_buf[pgm_index++] = (t>>8) & 0xff;
		pgm_buf[pgm_index++] = (t>>16) & 0xff;
		pgm_buf[pgm_index++] = tm & 0xff;
		pgm_buf[pgm_index++] = ((tm >> 8) << 4) | (p & 0x0f);
		pgm_buf[pgm_index++] = ((p >> 2) & 0xfc) | (h & 0x03);
		pgm_buf[pgm_index++] = h >> 2;

 		pgm_buf[pgm_index++] = lat >> 8;
		pgm_buf[pgm_index++] = lat & 0xff;
		pgm_buf[pgm_index++] = lat_m >> 8;
		pgm_buf[pgm_index++] = lat_m & 0xff;
 		pgm_buf[pgm_index++] = lon >> 8;
		pgm_buf[pgm_index++] = lon & 0xff;
		pgm_buf[pgm_index++] = lon_m >> 8;
		pgm_buf[pgm_index++] = lon_m & 0xff;
		pgm_buf[pgm_index++] = height >> 8;
		pgm_buf[pgm_index++] = height & 0xff;
		pgm_buf[pgm_index++] = co >> 8;
		pgm_buf[pgm_index++] = co & 0xff;

		time_eeprom = 0;
		if (pgm_index >= 13*19) {
			write_pgm_now = 1;
			pgm_index = 0;
		}
	}
}

ISR(USART0_UDRE_vect)
{
	if (PINE & (1<<3)) { // module is busy, abort the current transfer
		UCSR0B &= ~(1<<UDRIE0);
		output_index = 0;
		return;
	}
	if (!output_index) {
		uint32_t t = time_ms;
		uint16_t tm = temperature, p = pressure, h = humidity;
		output_buf[2] = t & 0xff;
		output_buf[3] = (t>>8) & 0xff;
		output_buf[4] = (t>>16) & 0xff;
		output_buf[5] = tm & 0xff;
		output_buf[6] = ((tm >> 8) << 4) | (p & 0x0f);
		output_buf[7] = ((p >> 2) & 0xfc) | (h & 0x03);
		output_buf[8] = h >> 2;

 		output_buf[9] = lat >> 8;
		output_buf[10] = lat & 0xff;
		output_buf[11] = lat_m >> 8;
		output_buf[12] = lat_m & 0xff;
 		output_buf[13] = lon >> 8;
		output_buf[14] = lon & 0xff;
		output_buf[15] = lon_m >> 8;
		output_buf[16] = lon_m & 0xff;
		output_buf[17] = height >> 8;
		output_buf[18] = height & 0xff;
		output_buf[19] = co >> 8;
		output_buf[20] = co & 0xff;
	}
	UDR0 = output_buf[output_index];
	output_index = (output_index+1) % BUF_SIZE;
}

ISR(USART1_RX_vect)
{
	static enum { WD, WG, WP, WG2, WG3, WA, W1C, W2C,
		      LAT11, LAT12, LAT21, LAT22,
		      WDOT,
		      LAT31, LAT32, LAT41, LAT42,
		      W3C, W4C, WDUMMY,
		      LON11, LON12, LON21, LON22,
		      WDOT2,
		      LON31, LON32, LON41, LON42,
		      W5C, W6C, W7C, W8C, W9C,
		      HEIGHT,
		      SENDING } gps_state=WD;
	static uint16_t acc_height;
	char c = UDR1;
	switch (gps_state) {
	case WD: if (c == '$') gps_state++; break;
	case WG: if (c == 'G') gps_state++; else gps_state=WD; break;
	case WP: if (c == 'P') gps_state++; else gps_state=WD; break;
	case WG2: if (c == 'G') gps_state++; else gps_state=WD; break;
	case WG3: if (c == 'G') gps_state++; else gps_state=WD; break;
	case WA: if (c == 'A') gps_state++; else gps_state=WD; break;
	case W1C:
	case W2C:
		if (c == ',')
			gps_state++;
		else if (c == '\n')
			gps_state=WD;
		break;
	case LAT11:
		gps_state++;
		lat = (c - '0') << 12;
		break;
	case LAT12:
		gps_state++;
		lat |= (c - '0') << 8;
		break;
	case LAT21:
		gps_state++;
		lat |= (c - '0') << 4;
		break;
	case LAT22:
		gps_state++;
		lat |= (c - '0');
		break;
	case WDOT:
		gps_state++;
		break;
	case LAT31:
		gps_state++;
		lat_m = (c - '0') << 12;
		break;
	case LAT32:
		gps_state++;
		lat_m |= (c - '0') << 8;
		break;
	case LAT41:
		gps_state++;
		lat_m |= (c - '0') << 4;
		break;
	case LAT42:
		gps_state++;
		lat_m |= (c - '0');
		break;
	case W3C:
	case W4C:
		if (c == ',')
			gps_state++;
		else if (c == '\n')
			gps_state=WD;
		break;
	case WDUMMY:
		gps_state++;
		break;
	case LON11:
		gps_state++;
		lon = (c - '0') << 12;
		break;
	case LON12:
		gps_state++;
		lon |= (c - '0') << 8;
		break;
	case LON21:
		gps_state++;
		lon |= (c - '0') << 4;
		break;
	case LON22:
		gps_state++;
		lon |= (c - '0');
		break;
	case WDOT2:
		gps_state++;
		break;
	case LON31:
		gps_state++;
		lon_m = (c - '0') << 12;
		break;
	case LON32:
		gps_state++;
		lon_m |= (c - '0') << 8;
		break;
	case LON41:
		gps_state++;
		lon_m |= (c - '0') << 4;
		break;
	case LON42:
		gps_state++;
		lon_m |= (c - '0');
		break;
	case W5C:
	case W6C:
	case W7C:
	case W8C:
	case W9C:
		if (c == ',')
			gps_state++;
		else if (c == '\n')
			gps_state=WD;
		break;
	case HEIGHT:
		if (c == '.')
			gps_state++;
		else {
			acc_height *= 10;
			acc_height += c - '0';
		}
		break;
	case SENDING:
		if (c == '\r') {
			gps_state = WD;
			height = acc_height;
			acc_height = 0;
		}
		break;
	}
}

static __attribute__((__noinline__)) BOOTLOADER_SECTION void boot_program_page(uint32_t page, uint8_t *buf)
{
        uint16_t i;
        uint8_t sreg;

        // Disable interrupts.

        sreg = SREG;
        cli();
    
        eeprom_busy_wait ();

        // boot_page_erase (page);
        boot_spm_busy_wait ();      // Wait until the memory is erased.

        for (i=0; i<SPM_PAGESIZE; i+=2)
        {
            // Set up little-endian word.

            uint16_t w = *buf++;
            w += (*buf++) << 8;
        
            boot_page_fill (page + i, w);
        }

        boot_page_write (page);     // Store buffer in flash page.
        boot_spm_busy_wait();       // Wait until the memory is written.

        // Reenable RWW-section again. We need this if we want to jump back
        // to the application after bootloading.

        boot_rww_enable ();

        // Re-enable interrupts (if they were ever enabled).

        SREG = sreg;
}

static void uart_putc(uint8_t c)
{
	while (!(UCSR0A & (1<<UDRE0)))
		;
	UDR0 = c;
}

static void init_uart()
{
#define BAUD 38400
#include <util/setbaud.h>
	UBRR0H = UBRRH_VALUE;
	UBRR0L = UBRRL_VALUE;
#if USE_2X
	UCSR0A |= (1 << U2X);
#else
	UCSR0A &= ~(1 << U2X);
#endif
#undef BAUD
	UCSR0B = (1<<TXEN);
	UCSR0C = (1<<UCSZ01) | (1<<UCSZ00);
#define BAUD 9600
#include <util/setbaud.h>
	UBRR1H = UBRRH_VALUE;
	UBRR1L = UBRRL_VALUE;
#if USE_2X
	UCSR1A |= (1 << U2X);
#else
	UCSR1A &= ~(1 << U2X);
#endif
#undef BAUD
	UCSR1B = (1<<RXEN);
	UCSR1C = (1<<UCSZ01) | (1<<UCSZ00);
	UCSR1B |= (1<<RXCIE1);

	PORTE &= ~(1<<PE2);
	_delay_ms(100);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
	uart_putc(0x7e);
//	uart_putc(35); // 425.9 MHz
//	uart_putc(107); // 433.1 MHz
	uart_putc(99); // 432.3 MHz
	uart_putc(3);
	uart_putc(30);
	uart_putc(30);
	_delay_ms(100);
	PORTE |= (1<<PE2);
}

#if 0
static inline void press_button(double ms)
{
	DDRE |= (1<<PE4);
	_delay_ms(ms);
	DDRE &= ~(1<<PE4);
}

static void start_recording()
{
	press_button(3000);
	_delay_ms(3000);
	press_button(500);
	_delay_ms(3000);
	press_button(3000);
}
#endif

static void init_ow()
{
	uint8_t diff = OW_SEARCH_FIRST;
	DS18X20_find_sensor(&diff, id);
}

static void init_hw()
{
	DDRE |= (1<<PE2) | (1<<PE4);
	ADCSRA |= (1<<ADEN) | (1<<ADPS2) | (1<<ADPS1); // 64 div, 125kHz freq

	TCCR1B |= (1<<CS10) | (1<<WGM12); // CTC mode, 1000 Hz, no prescaling
	OCR1A = 8000;
	TIMSK |= (1<<OCIE1A);

	PORTG |= (1<<PG3) | (1<<PG4);
	DDRG |= (1<<PG3) | (1<<PG4);

	init_uart();
	init_ow();
}

static void meas_temp()
{
	ow_reset();
	ow_command(DS18X20_CONVERT_T, id);
}

static void read_temp()
{
	uint8_t sp[DS18X20_SP_SIZE];
        
        if (read_scratchpad(id, sp, DS18X20_SP_SIZE) == DS18X20_OK)
                temperature = sp[1]<<8 | sp[0];
}

int main()
{
	init_hw();

/*	start_recording();*/
	_delay_ms(1000);
	first_boot = eeprom_read_byte(&first_boot);
	page_addr = 0x1fb00;
	eeprom_write_byte(&first_boot, 0);

	sei();
	while (1) {
		meas_temp();
		read_temp();
		if (write_pgm_now && first_boot) {
			PORTE |= (1<<4);
			boot_program_page(page_addr, pgm_buf);
			time_ms += 4;
			PORTE &= ~(1<<4);
			write_pgm_now = 0;
			page_addr -= 256;
			if (page_addr < 256*15)
				first_boot = 0;
		}
		UCSR0B |= (1<<UDRIE0); // enable transmit in case it was stopped by CTS
	}

	return 0;
}

